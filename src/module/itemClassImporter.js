import registerInventoryItems from "./items/inventoryItem.js";
import registerStates from "./items/states.js";
import registerConditions from "./items/conditions.js";
import registerAdvantages from "./items/advantages.js";

/**
 * The common method to import all item implementations into foundry.
 */
export default function importItemClasses() {
    registerInventoryItems();
    registerStates();
    registerConditions();
    registerAdvantages();
}
