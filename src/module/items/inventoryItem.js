class ThiefsKnife extends TDE5E.itemTypeClasses.inventoryItem.default {
    /** @override */
    canModifyRoll(item) {
        return item.system.cid === "SCHLOSSERKNACKEN";
    }

    /** @override */
    modifyRoll() {
        return new TDE5E.rollModifications.AttributeModification(this, 1, { active: true });
    }
}

class ThiefsMantle extends TDE5E.itemTypeClasses.inventoryItem.default {
    /** @override */
    canModifyRoll(item) {
        return item.system.cid === "VERBERGEN";
    }

    /** @override */
    modifyRoll() {
        return new TDE5E.rollModifications.RatingModification(this, 1, { active: true });
    }
}

class ThiefsBoots extends TDE5E.itemTypeClasses.inventoryItem.default {
    /** @override */
    canModifyRoll(item) {
        return item.system.cid === "VERBERGEN";
    }

    /** @override */
    modifyRoll() {
        return new TDE5E.rollModifications.AttributeModification(this, 1, { active: true });
    }
}

class RondrasFirmament extends TDE5E.itemTypeClasses.inventoryItem.default {
    /** @override */
    canModifyRoll(item) {
        if (item.system.cid === "WILLENSKRAFT") return true;
        const attributes = item.system.check?.attributes;
        if (!attributes) return false;
        const hasConstitution = TDE5E.helpers.countOccurences(attributes, "constitution") > 0;
        const hasNoCourage = TDE5E.helpers.countOccurences(attributes, "courage") === 0;
        return hasConstitution && hasNoCourage;
    }

    /** @override */
    modifyRoll(item) {
        if (item.system.cid === "WILLENSKRAFT") return this._modifyWillpower(item);
        return this._modifyCourage(item);
    }

    /** @override */
    prepareActorData() {
        this.actor.modify(
            this.displayName,
            this.actor.system.resources.adrenaline,
            "max",
            Math.max(this.actor.system.attributes.courage.base - 10, 0),
        );
        this.actor.uniqueItems.get("FURCHT")?.modifyValue(this.name, -1);
    }

    /**
     *  Modifies the roll for willpower, adding 3 to the rating (on success).
     * @returns {object} A modification.
     */
    _modifyWillpower() {
        return new TDE5E.rollModifications.SkillPointModification(this, 3);
    }

    /**
     *  Modifies the roll for check that contain constitution but no courage,
     *   replacing the first constitution with  courage.
     *  @param {object} item item.
     *  @returns {object} A modification.
     */
    _modifyCourage(item) {
        const index = item.system.check?.attributes.indexOf("constitution");
        const { attributes } = this.actor.system;
        const isActive = attributes.courage.value >= attributes.constitution.value;
        return new TDE5E.rollModifications.ReplaceAttributeModification(
            this,
            this.actor.system.attributes.courage.value,
            index,
            "courage",
            { active: isActive },
        );
    }
}

class NimbusBoots extends TDE5E.itemTypeClasses.inventoryItem.default {
    /** @override */
    canModifyRoll(item) {
        return item.system.cid === "ATHLETIK";
    }

    /** @override */
    modifyRoll() {
        return new TDE5E.rollModifications.AttributeModification(this, 1, { active: false });
    }
}

class IronTeethBoots extends TDE5E.itemTypeClasses.inventoryItem.default {
    /** @override */
    canModifyRoll(item) {
        return item.system.cid === "KLETTERN";
    }

    /** @override */
    modifyRoll() {
        return new TDE5E.rollModifications.RatingModification(this, 1, { active: false });
    }
}
/**
 * The common method to import all item implementations into foundry.
 */
export default function registerInventoryItems() {
    const { inventoryItem } = TDE5E.itemTypeClasses;
    inventoryItem.DIEBESMESSER = ThiefsKnife;
    inventoryItem.DIEBESMANTEL = ThiefsMantle;
    inventoryItem.DIEBESSCHUHE = ThiefsBoots;
    inventoryItem.RONDRAS_FIRMAMENT = RondrasFirmament;
    inventoryItem.NIMBUS_SCHUHE = NimbusBoots;
    inventoryItem.EISENZAHNE = IronTeethBoots;
}
