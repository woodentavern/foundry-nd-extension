class WormInfection extends TDE5E.itemTypeClasses.advantage.default {
    preparationPriority = TDE5E.preparationPriority.BEFORE_ATTRIBUTES;

    /** @inheritdoc */
    canModifyRoll() {
        return false;
    }

    /** @override */
    prepareActorData() {
        super.prepareActorData();
        ["dexterity", "agility", "constitution", "strength"]
            .forEach(entry => this.actor.modify(this.name, this.actor.system.attributes[entry], "base", 1));
    }
}

/**
 * Registers the items for this type.
 */
export default function registerAdvantages() {
    TDE5E.itemTypeClasses.advantage.WURMINFEKTION = WormInfection;
}
