class LycanthropsBurden extends TDE5E.itemTypeClasses.condition.default {
    preparationPriority = TDE5E.preparationPriority.AFTER_MODIFIED;

    /** @inheritdoc */
    canModifyRoll() {
        return false;
    }

    /** @override */
    prepareActorData() {
        super.prepareActorData();
        this.actor.modify(this.name, this.actor.system.resources.lifePoints, "max", this.value * -2);
    }

    /** @inheritdoc */
    get currentEffectShortText() {
        return (this.value > 0)
            ? `-${this.value * 2} ${game.i18n.localize("tde5e.actor.resources.lifePointsShort")}`
            : "-";
    }
}

/**
 * Registers the items for this type.
 */
export default function registerConditions() {
    TDE5E.itemTypeClasses.condition.LAST_DES_LYKANTHROPEN = LycanthropsBurden;
}
