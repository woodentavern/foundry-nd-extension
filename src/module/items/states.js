// class WerebearForm extends TDE5E.itemTypeClasses.state.default {
//     // CURRENTLY DISABLED
//     preparationPriority = TDE5E.preparationPriority.BEFORE_ATTRIBUTES;

//     /** @override */
//     prepareBaseData() {
//         super.prepareBaseData();
//         // Add an action
//         if (this.actor?.system) {
//             const baseActions = foundry.utils.getProperty(this.actor, "system.settings.combatantCount") ?? 1;
//             const werebearActions = this.actionCount;
//             if (baseActions < werebearActions) {
//                 foundry.utils.setProperty(this.actor.system, "settings.combatantCount", werebearActions);
//             }
//         }
//     }

//     /** @inheritdoc */
//     canModifyRoll(item) {
//         return this.flags.tde5e?.transformation?.skills?.hasOwnProperty(item.data._source.data.cid);
//     }

//     /** @inheritdoc */
//     modifyRoll(item) {
//         if (!this.active) return null;
//         return new TDE5E.rollModifications.RatingModification(this, this.getSkillMod(item._source.system.cid));
//     }

//     /** @override */
//     prepareActorData() {
//         if (!this.active) return;
//         Object.entries(this.actor.system.attributes)
//             .forEach(entry => this.actor.modify(this.name, entry[1], "base", this.getAttributeModData(entry[0])));

//         this.actor.modifyLater(
//             this.name,
//             this.actor.system.resources.lifePoints,
//             "max",
//             this.lifepointMod,
//             TDE5E.preparationPriority.AFTER_MODIFIED
//         );
//         this.actor.modifyLater(this.name, this.actor.system.derived.movement, "value", this.movementMod);
//     }

//     /**
//      * @override
//      */
//     static async allowCreation(data, actor) {
//         if (TDE5E.utility.hasVariants(data)) await TDE5E.utility.promptVariant(data, actor);

//         if (!data.system.cid) return true;

//         // Undo the variant
//         data.system.cid = (data.system.cid.includes(":"))
//             ? data.system.cid.split(":")[0]
//             : data.system.cid;

//         const existing = actor.uniqueItems.get(data.system.cid);
//         const allow = existing ? await TDE5E.utility.promptReplace(existing) : true;

//         if (!allow) util.error(`Can't add item ${data.name} because it already exists.`, false, true);
//         return allow;
//     }

//     getSkillMod(skill) {
//         if (!this.active) return 0;
//         return this.flags.tde5e?.transformation?.skills[skill] ?? 0;
//     }

//     getAttributeModData(attribute) {
//         if (!this.active) return 0;
//         return this.flags.tde5e?.transformation?.attributes[attribute]?.base ?? 0;
//     }

//     get lifepointMod() {
//         if (!this.active) return 0;
//         return this.flags.tde5e?.transformation?.resources?.lifepoints?.mod ?? 0;
//     }

//     get movementMod() {
//         if (!this.active) return 0;
//         return this.flags.tde5e?.transformation?.derived?.movement?.mod ?? 0;
//     }

//     get actionCount() {
//         if (!this.active) return 1;
//         return this.flags.tde5e?.transformation?.other?.actions ?? 1;
//     }

//     get protectionMod() {
//         if (!this.active) return 0;
//         return this.flags.tde5e?.transformation?.other?.protection ?? 0;
//     }

//     get unarmedDamageBonus() {
//         if (!this.active) return 0;
//         return this.flags.tde5e?.transformation?.other?.damage ?? 0;
//     }
// }
/**
 * The common method to import all item implementations into foundry.
 */
export default function registerStates() {
    // TDE5E.itemTypeClasses.state["WERBARGESTALT"] = WerebearForm;

}
