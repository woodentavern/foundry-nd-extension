import importItemClasses from "./module/itemClassImporter.js";

/** Hook to register settings. */
Hooks.once("init", async () => {
    // eslint-disable-next-line no-console -- We need no specific helper
    console.log("nd-tde5e-ext | Initializing nd-tde5e-ext");
    importItemClasses();
});
