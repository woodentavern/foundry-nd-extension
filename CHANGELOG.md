10.1
- Migration to Foundry V10 
- Fixed a bug where variant functions were not imported

1.0.0
- Core functionality.